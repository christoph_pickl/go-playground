# go-playground

this project is all about learning some go.

focus on the following topics:

* concurrency
* tests (benchmark support)

## best practices

* Use the built-in error interface type, rather than a concrete type, to store and return error values.

## further reading

howtos:

- official doc: https://github.com/golang/go/wiki
- comprehensive tutorial: https://golang.org/doc/effective_go.html

details:

- from java to go: https://yourbasic.org/golang/go-java-tutorial/