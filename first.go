package main

import (
	"./otherpkg"
	"fmt"
	"math/rand"
)

func main() {
	fmt.Println("Some random number: ", rand.Intn(10))
	var p1 Person
	p1.name = "World!"
	p1.Greet("Hi")

	fmt.Println("otherpkg.Visible", otherpkg.Visible)

	arr1 := make([]int, 2)
	fmt.Println("Array length:", len(arr1))

	foo(1)
	foo(true)
	foo("x")

	fmt.Println(Sum())        // 0
	fmt.Println(Sum(1, 2, 3)) // 6
}

type Person struct {
	name string
}

func (p *Person) Greet(prefix string) {
	fmt.Println(prefix, ",", p.name)
}

func foo(arg interface{}) {
	switch f := arg.(type) {
	case bool:
		PrintBool(f)
	case int:
		fmt.Println("Int:", arg)
	default:
		fmt.Println("Unsupported:", arg)
	}
}
func PrintBool(b bool) {
	fmt.Println("its a bool:", b)
}

// Variadic functions
func Sum(nums ...int) int {
	res := 0
	for _, n := range nums {
		res += n
	}
	return res
}
