package main

import (
	"fmt"
	"strconv"
)

type Stringable interface {
	String() string
}

type Euro int

func (e Euro) String() string {
	return strconv.Itoa(int(e)) + " EUR"
}

type Point struct {
	x, y int
}

func (p *Point) String() string {
	return fmt.Sprintf("<%d,%d>", p.x, p.y)
}

func main() {
	var x Stringable

	x = Euro(24)
	fmt.Println(x.String()) // 24 EUR

	x = &Point{1, 2}
	fmt.Println(x.String()) // <1,2>
	// %v ... the concrete value; %T ... the dynamic type
	fmt.Printf("Value: %v   Type: %T\n", x, x) // <1,2> *main.Point

	var y = Foo{1, "name"}
	fmt.Printf("Value: %v   Type: %T\n", y, y) // <1,2> *main.Point
}

type Foo struct {
	bar  int
	name string
}

func (f *Foo) String() string {
	return "like override toString()"
}
