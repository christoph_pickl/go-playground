package main

import "fmt"

func main() {
	s := []string{"Foo", "Bar"}
	for i, v := range s {
		fmt.Println(i, v)
		// 0 Foo
		// 1 Bar
	}

	// need to "make" it, otherwise null pointer (still reads as empty map, but panics on write)
	m := make(map[string]int)
	m["a"] = 1
	m["b"] = 2
	for i, v := range m {
		fmt.Println(i, v)
		// a 1
		// b 2
	}
	fmt.Println(m["c"]) // default value of: 0 (not nil!), would need: map[string]*int
}
