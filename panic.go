package main

import (
	"fmt"
	"reflect"
)

func main() {
	n := ohnoes()
	fmt.Println("main received", n)

	fmt.Println("panics:", Panics(func() {
		//panic("will be true")
	}))
}

// => main receives 0
//func ohnoes() int {
//	defer func() {
//		if err := recover(); err != nil {
//			fmt.Println(err)
//		}
//	}()
//	m := 1
//	panic("ohnoes: panic!")
//	m = 2
//	return m
//}

// => main receives: 2
func ohnoes() (m int) {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println(err)
			//debug.PrintStack()
			m = 2
		}
	}()
	m = 1
	panic("ohnoes: panic!")
	m = 3
	return m
}

// Panics tells if function f panics with parameters p.
func Panics(f interface{}, p ...interface{}) bool {
	fv := reflect.ValueOf(f)
	ft := reflect.TypeOf(f)
	if ft.NumIn() != len(p) {
		panic("wrong argument count")
	}
	pv := make([]reflect.Value, len(p))
	for i, v := range p {
		if reflect.TypeOf(v) != ft.In(i) {
			panic("wrong argument type")
		}
		pv[i] = reflect.ValueOf(v)
	}
	return call(fv, pv)
}

func call(fv reflect.Value, pv []reflect.Value) (b bool) {
	defer func() {
		if err := recover(); err != nil {
			b = true
		}
	}()
	fv.Call(pv)
	return
}
