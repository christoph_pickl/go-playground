package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

// Instead of exceptions, Go uses errors to signify events such as end-of-file,
// and run-time panics for run-time errors such as attempting to index an array out of bounds.

//type error interface {
//	Error() string
//}

type InternalError struct {
	Path string
}

func (e *InternalError) Error() string {
	return fmt.Sprintf("I/O error: %v", e.Path)
}

func main() {
	fmt.Println("START")
	fileName := "/Users/cp/Workspace/private/go-playground/nil.go"

	file2, err2 := read(fileName)
	check(err2)

	fmt.Println("Read content:", file2)

	fmt.Println("END")
	// err := errors.New("Houston, we have a problem")
	// err2 := fmt.Errorf("math: square root of negative number %g", x)
}

func read(fileName string) (string, error) {
	fmt.Println("READ")
	_, err := os.Open(fileName)
	if err != nil {
		//log.Fatal(err)
		return "", &InternalError{fileName}
	}

	dat, err := ioutil.ReadFile(fileName)
	check(err)
	fileContent := string(dat)
	//fmt.Print(fileContent)
	return fileContent, nil
}

func check(e error) {
	if e != nil {
		//panic(e)
		fmt.Println("Error occured:", e)
		//fmt.Println("ERROR reading from:", err2.Path)
		log.Fatal(e)
	}
}
