package main

import (
	"fmt"
	"os"
)

func Bar() error {
	var err *os.PathError = nil
	// …
	return err
}

func main() {
	err := Bar()

	fmt.Println(err) // <nil>

	fmt.Println(err == nil) // false
	// An interface value is equal to nil only if both its value and dynamic type are nil.
	// In the example above, Foo() returns [nil, *os.PathError] and we compare it with [nil, nil].

	fmt.Println(err == (*os.PathError)(nil)) // true
}
